
# 介绍

通过调用企业微信群机器人 API 实现向企业微信群发送文本（text）、Markdown、图片（image）、图文（news）、文件（file）、语音（voice）、模板卡片（template_card）消息。可以配合 GitLab CI/CD 和定时任务实现办公自动化，比较典型的应用是每日定时向群里发送日报信息。

群机器人配置说明：https://developer.work.weixin.qq.com/document/path/91770

## 安装

```r
# 从 GitLab 仓库安装开发版
remotes::install_gitlab("chuxinyuan/tellwx")
# Install from CRAN
install.packages("tellwx")
```

## 设置机器人 webhook

首选需要在企业微信里添加群机器人，添加完成后会生成一个 Webhook 地址，这个地址里 “?key=” 后面的那一串就是 KEY 了。

把这个 KEY 放在下面的代码里运行一次即可。

```r
cat(
  '\n# Enterprise Wechat KEY',
  'ENTERPRISE_WECHAT_KEY = "Your Enterprise Wechat KEY"',
  file = '~/.Renviron', sep = '\n', append = TRUE
)
```

## 使用说明

### 发送文本消息

```r
wx_text("Hello world!")
```

默认群发，支持@群成员。

### 发送 Markdown 文本消息

```r
wx_md(
   "实时新增用户反馈<font color=\"warning\">132例</font>，请相关同事注意。\n
    >类型:<font color=\"comment\">用户反馈</font>
    >普通用户反馈:<font color=\"comment\">117例</font>
    >VIP用户反馈:<font color=\"comment\">15例</font>"
)
```

默认群发，支持@群成员。

### 发送图片

```r
wx_img("path/to/xxxx.png")
```

图片（base64 编码前）最大不能超过 2M，支持 JPG 和 PNG 格式。

### 发送图文消息

```r
wx_articles(
  title = "中秋节礼品领取",
  desc = "今年中秋节公司有豪礼相送",
  url = "https://www.qq.com/",
  picurl = "http://res.mail.qq.com/node/ww/wwopenmng/images/independent/doc/test_pic_msg1.png"
)
```

这里的图片不是本地图片，而是是图片链接。较好的效果为大图 1068x455，小图 150x150。

### 发送文件

```r
wx_file("path/to/xxxx.csv")
```

这里的文件不包含 `.AMR` 格式的语音文件。文件大小均要求大于 5 个字节，但文件大小不超过 20M。

### 发送语音文件

```r
wx_voice("path/to/xxxx.AMR")
```

语音文件要求大于 5 个字节，但大小不超过2M，播放长度不超过60s，仅支持 AMR 格式。

### 发送模版卡片类消息

有两种不同的卡片类型，“text_notice” 和 “news_notice”，参数设置略有不同。

- “text_notice” 类型：“card_image” 和 “image_text_area” 参数不可用。
- “news_notice” 类型：“emphasis_content” 和 “sub_title_textand” 参数为不可用。

```r
# 示例一
wx_card(
  card_type = "text_notice",
  source = list(
    icon_url = "https://wework.qpic.cn/wwpic/252813_jOfDHtcISzuodLa_1629280209/0",
    desc = "企业微信",
    desc_color = 0
  ),
  main_title = list(
    title = "欢迎使用企业微信",
    desc = "您的好友正在邀请您加入企业微信"
  ),
  emphasis_content = list(
    title = "100",
    desc = "数字含义"
  ),
  quote_area = list(
    type = 1,
    url = "https://work.weixin.qq.com/?from=openApi",
    appid = "APPID",
    pagepath = "PAGEPATH",
    title = "引用文本标题",
    quote_text = "Jack：企业微信真的很好用~\nBalian：超级好的一款软件！"
  ),
  sub_title_text = "下载企业微信还能抢红包！",
  vertical_content_list = list(
    list(
      title = "惊喜红包等你来拿",
      desc = "下载企业微信还能抢红包！"
    )
  ),
  horizontal_content_list = list(
    list(
      keyname = "邀请人",
      value = "张三"
    ),
    list(
      keyname = "企微官网",
      value = "点击访问",
      type = 1,
      url = "https://work.weixin.qq.com/?from=openApi"
    ),
    list(
      keyname = "图片下载",
      value = "file basename",
      type = 2,
      media_id = get_media_id("path/to/file")
    )
  ),
  jump_list = list(
    list(
      type = 1,
      url = "https://work.weixin.qq.com/?from=openApi",
      title = "企业微信官网"
    )
    # list(
    #   type = 2,
    #   appid = "APPID",
    #   pagepath = "PAGEPATH",
    #   title = "跳转小程序"
    # )
  ),
  card_action = list(
    type = 1,
    url = "https://work.weixin.qq.com/?from=openApi",
    appid = "APPID",
    pagepath = "PAGEPATH"
  )
)

# 示例二
wx_card(
  card_type = "news_notice",
  source = list(
    icon_url = "https://wework.qpic.cn/wwpic/252813_jOfDHtcISzuodLa_1629280209/0",
    desc = "企业微信",
    desc_color = 0
  ),
  main_title = list(
    title = "欢迎使用企业微信",
    desc = "您的好友正在邀请您加入企业微信"
  ),
  card_image = list(
    url = "https://wework.qpic.cn/wwpic/354393_4zpkKXd7SrGMvfg_1629280616/0",
    aspect_ratio = 2.25
  ),
  image_text_area = list(
    type = 1,
    url = "https://work.weixin.qq.com",
    title = "欢迎使用企业微信",
    desc = "您的好友正在邀请您加入企业微信",
    image_url = "https://wework.qpic.cn/wwpic/354393_4zpkKXd7SrGMvfg_1629280616/0"
  ),
  quote_area = list(
    type = 1,
    url = "https://work.weixin.qq.com/?from=openApi",
    appid = "APPID",
    pagepath = "PAGEPATH",
    title = "引用文本标题",
    quote_text = "Jack：企业微信真的很好用~\nBalian：超级好的一款软件！"
  ),
  vertical_content_list = list(
    list(
      title = "惊喜红包等你来拿",
      desc = "下载企业微信还能抢红包！"
    )
  ),
  horizontal_content_list = list(
    list(
      keyname = "邀请人",
      value = "张三"
    ),
    list(
      keyname = "企微官网",
      value = "点击访问",
      type = 1,
      url = "https://work.weixin.qq.com/?from=openApi"
    ),
    list(
      keyname = "图片下载",
      value = "file basename",
      type = 2,
      media_id = get_media_id("path/to/file")
    )
  ),
  jump_list = list(
    list(
      type = 1,
      url = "https://work.weixin.qq.com/?from=openApi",
      title = "企业微信官网"
    )
    # list(
    #   type = 2,
    #   appid = "APPID",
    #   pagepath = "PAGEPATH",
    #   title = "跳转小程序"
    # )
  ),
  card_action = list(
    type = 1,
    url = "https://work.weixin.qq.com/?from=openApi",
    appid = "APPID",
    pagepath = "PAGEPATH"
  )
)
```

上面的代码包含上传文件，因此需要根据实际情况修改代码中的参数，其中：

```r
list(
  keyname = "图片下载",
  value = "file basename",
  type = 2,
  media_id = get_media_id("path/to/file")
)
```

如果文件的路径为 `./assets/xxxx.png`，那么需要将上面的 `path/to/file` 替换成  `./assets/xxxx.png`，`value` 的参数值为 `xxxx.png`。

## 软件许可

tellwx 是开源免费软件, 以 MIT + file LICENSE 方式授权.
